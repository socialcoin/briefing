# Socialcoin

[![N|Socialcoin](https://www.socialcoin.nl/wp-content/uploads/2017/03/cropped-SC-logo-corner-1.jpg)](https://www.socialcoin.nl/)

SOCIALCOIN. NIEUWE WIJZE OM SCHULDEN AF TE LOSSEN.
Meer info en achtergrond op http://socialcoin.nl

# Status
Er is een bestaande applicatie in Angular, versie vroeger, die niet makkelijk bij te werken is.

# Requirements
- De bestaande API moet gehandhaafd blijven.
- Er is zijn twee soorten gebruikers: Administrators en Participants. Paricipants zijn gebruikers van de schuldeisers, schuldhulpverleners, gemeente. Participants zijn de schuldenaren.
- Administrators gebruiken uitsluitend een **desktop PC**
- Parcicipants gebruiken uitsluitend een **mobiele telefoon**. Een web app is voldoende. Een native app is niet gewenst.
- API
    -  REST interface
    -  Gebruik tooling zoals bijv. van swagger.io
    - Tip: RDBMS, bijv MySQL of Postgres
    - Middleware volgens de course
- Tests
    - API tests (wordt aangeleverd)
    - E2E tests (wordt aangeleverd)
- Deployment
    - Container voor front end / administrators
    - Container voor front end / participants
    - Container(s) voor backend


# Resultaat
## Afzonderlijke apps voor
Het eindresultaat, maar ook development, kan in de volgende componenten opgesplitst worden.
- Storage, inclusief datamodel en import script
- Backend, inclusief API
- Desktop app. Dit is de web app voor desktop computers met liggende schermen voor administrators.
- Mobile web app. Dit is de web app voor mobiele telefoons met participants als gebruikers.

## Tests
Tests zijn inclusief startup en tear down stappen. Tip: zorg er vanaf het begin voor dat je testbare code oplevert. 
- API tests voor elke API
- E2E tests voor elk scherm en elke interactie

## Automatisch deployment
- Bij voorkeur scripted installaties zoals Ansible

## Gewenste documentatie
- Uitsluitend Engelstalige keywords en comments in de source code
- Uitsluitend Engelstalige voor de overdrachtsdocumentatie (ivm niet-Nederlandstalige developers)
  - Design documentatie
  - Deployment documentatie
  
# UI    
## Vereist
- Meertaligheid zodat in de toekomst externe, niet-Nederlandstalige developers er aan verder kunnen werken

## Gewenst
Framework

Check populariteit op [stackoverflow](https://insights.stackoverflow.com/trends?tags=reactjs%2Cangular%2Cvue.js)
- Vue.js (voorkeur)
- React.js
- Angular.js

Design

Check populariteit op bij [npmtrends](https://www.npmtrends.com/element-react-vs-element-ui-vs-material-ui)
- Material-ui (gewenst)
- Element-ui


## De schermen voor de Administrators zijn:
- CRU (Create, Retrieve, Update) voor gebruikers
- CRU voor taken
- CRU voor teams
- CRU voor Participants en taken

## De schermen voor Participants zijn
- CR voor taken
- CR voor activiteiten
- C voor maken van foto met camera van de mobiel 