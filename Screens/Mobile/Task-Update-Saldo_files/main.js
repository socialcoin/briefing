(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/webpack/hot sync ^\\.\\/log$":
/*!*************************************************!*\
  !*** (webpack)/hot sync nonrecursive ^\.\/log$ ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./log": "./node_modules/webpack/hot/log.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	var module = __webpack_require__(id);
	return module;
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/webpack/hot sync ^\\.\\/log$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/_directives/alert/alert.component.html":
/*!********************************************************!*\
  !*** ./src/app/_directives/alert/alert.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"message\" [ngClass]=\"{ 'alert': message, 'alert-success': message.type === 'success', 'alert-danger': message.type === 'error' }\">\n  {{ message.text }}\n</div>"

/***/ }),

/***/ "./src/app/_directives/alert/alert.component.scss":
/*!********************************************************!*\
  !*** ./src/app/_directives/alert/alert.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/_directives/alert/alert.component.ts":
/*!******************************************************!*\
  !*** ./src/app/_directives/alert/alert.component.ts ***!
  \******************************************************/
/*! exports provided: AlertComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertComponent", function() { return AlertComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_alert_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../_services/alert.service */ "./src/app/_services/alert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AlertComponent = /** @class */ (function () {
    function AlertComponent(alertService) {
        this.alertService = alertService;
    }
    AlertComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.alertService.getMessage().subscribe(function (message) {
            _this.message = message;
        });
    };
    AlertComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    AlertComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-alert',
            template: __webpack_require__(/*! ./alert.component.html */ "./src/app/_directives/alert/alert.component.html"),
            styles: [__webpack_require__(/*! ./alert.component.scss */ "./src/app/_directives/alert/alert.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_alert_service__WEBPACK_IMPORTED_MODULE_1__["AlertService"]])
    ], AlertComponent);
    return AlertComponent;
}());



/***/ }),

/***/ "./src/app/_guards/auth.guard.ts":
/*!***************************************!*\
  !*** ./src/app/_guards/auth.guard.ts ***!
  \***************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _models_access_token__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_models/access-token */ "./src/app/_models/access-token.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, _location) {
        this.router = router;
        this._location = _location;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        var currentUser = _models_access_token__WEBPACK_IMPORTED_MODULE_3__["AccessToken"].decode(localStorage.getItem('currentUser'));
        if (currentUser.id) {
            return true;
        }
        console.log('no permission to access');
        this.router.navigate(['/'], { queryParams: { returnUrl: state.url } });
        return false;
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/_helpers/logging.interceptor.ts":
/*!*************************************************!*\
  !*** ./src/app/_helpers/logging.interceptor.ts ***!
  \*************************************************/
/*! exports provided: LoggingInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoggingInterceptor", function() { return LoggingInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

 // fancy pipe-able operators
var MAX_SIZE = 50000;
var LoggingInterceptor = /** @class */ (function () {
    function LoggingInterceptor() {
    }
    LoggingInterceptor.prototype.intercept = function (request, next) {
        var msg = 'Response:\n';
        // tslint:disable-next-line:max-line-length
        console.log("Request:\n " + request.method + " " + request.urlWithParams + " " + (request.body != null ? '\n ' + JSON.stringify(request.body) : ''));
        return next.handle(request)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])(function (response) {
            if (response.type !== 0) {
                msg = msg + response['status'];
                if (response['body']) {
                    var body = JSON.stringify(response['body'], null, ' '.repeat(4));
                    if (body.length > MAX_SIZE) {
                        body = body.substring(0, MAX_SIZE) + '...';
                    }
                    msg = msg + '\n' + body;
                }
            }
            console.log(msg);
        }, function (error) {
            msg = 'Error Response:\n';
            if (error.type !== 0) {
                msg = msg + error['status'];
            }
            console.log(msg);
        }));
    };
    LoggingInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], LoggingInterceptor);
    return LoggingInterceptor;
}());



/***/ }),

/***/ "./src/app/_models/access-token.ts":
/*!*****************************************!*\
  !*** ./src/app/_models/access-token.ts ***!
  \*****************************************/
/*! exports provided: AccessToken */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccessToken", function() { return AccessToken; });
var AccessToken = /** @class */ (function () {
    function AccessToken() {
    }
    AccessToken.decode = function (json) {
        var accessToken = Object.create(AccessToken.prototype);
        return Object.assign(accessToken, JSON.parse(json));
    };
    return AccessToken;
}());



/***/ }),

/***/ "./src/app/_models/task.ts":
/*!*********************************!*\
  !*** ./src/app/_models/task.ts ***!
  \*********************************/
/*! exports provided: Task */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Task", function() { return Task; });
var Task = /** @class */ (function () {
    function Task() {
    }
    return Task;
}());



/***/ }),

/***/ "./src/app/_services/alert.service.ts":
/*!********************************************!*\
  !*** ./src/app/_services/alert.service.ts ***!
  \********************************************/
/*! exports provided: AlertService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return AlertService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AlertService = /** @class */ (function () {
    function AlertService(router) {
        var _this = this;
        this.router = router;
        this.subject = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.keepAfterNavigationChange = false;
        // clear alert message on route change
        router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationStart"]) {
                if (_this.keepAfterNavigationChange) {
                    // only keep for a single location change
                    _this.keepAfterNavigationChange = false;
                }
                else {
                    // clear alert
                    _this.subject.next();
                }
            }
        });
    }
    AlertService.prototype.success = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'success', text: message });
    };
    AlertService.prototype.error = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'error', text: message });
    };
    AlertService.prototype.getMessage = function () {
        return this.subject.asObservable();
    };
    AlertService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AlertService);
    return AlertService;
}());



/***/ }),

/***/ "./src/app/_services/authentication.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/_services/authentication.service.ts ***!
  \*****************************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _models_access_token__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_models/access-token */ "./src/app/_models/access-token.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http) {
        this.http = http;
        this.loggedIn = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](false || localStorage.getItem('currentUser') != null);
    }
    Object.defineProperty(AuthenticationService.prototype, "isLoggedIn", {
        get: function () {
            return this.loggedIn.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthenticationService.prototype, "accessToken", {
        get: function () {
            return _models_access_token__WEBPACK_IMPORTED_MODULE_5__["AccessToken"].decode(localStorage.getItem('currentUser'));
        },
        enumerable: true,
        configurable: true
    });
    AuthenticationService.prototype.login = function (username, password) {
        var _this = this;
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiUrl + "/Relations/login", { username: username, password: password })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["mergeMap"])(function (accessToken) {
            var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('access_token', accessToken.id).set('filter', "{\"roles\":true}");
            return _this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiUrl + "/Relations/" + accessToken.userId, { params: params })
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (user) {
                accessToken.roles = (user.roles.map(function (role) { return role.id; }));
                localStorage.setItem('currentUser', JSON.stringify(accessToken));
                _this.loggedIn.next(true);
                return accessToken;
            }));
        }));
    };
    AuthenticationService.prototype.logout = function () {
        var _this = this;
        return new rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"](function (result) {
            result.next(true);
            _this.loggedIn.next(false);
            if (localStorage.getItem('currentUser')) {
                var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('access_token', _this.accessToken.id);
                _this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiUrl + "/Relations/logout", {}, { params: params })
                    .subscribe(function () {
                    _this.loggedIn.next(false);
                    localStorage.removeItem('currentUser');
                });
            }
        });
    };
    AuthenticationService.prototype.requestPasswordReset = function (email) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiUrl + "/Relations/reset", { email: email })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function () {
            console.log('request password reset sucessful: ', email);
            return true;
        }));
    };
    AuthenticationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthenticationService);
    return AuthenticationService;
}());



/***/ }),

/***/ "./src/app/_services/task.service.ts":
/*!*******************************************!*\
  !*** ./src/app/_services/task.service.ts ***!
  \*******************************************/
/*! exports provided: TaskService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskService", function() { return TaskService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./authentication.service */ "./src/app/_services/authentication.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TaskService = /** @class */ (function () {
    function TaskService(http, authService) {
        this.http = http;
        this.authService = authService;
    }
    TaskService.prototype.getByParticipant = function (participantId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]()
            .set('access_token', this.authService.accessToken.id)
            .set('filter', "{\"where\": {\"relationId\": " + participantId + "}}");
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + "/Tasks", { params: params })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["flatMap"])(function (res) { return res; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (task) {
            if (task.actions.length > 0) {
                var startedActions = task.actions.filter(function (action) { return action.description === 'START'; });
                if (startedActions.length > 0) {
                    task.taskStartDate = startedActions[0].startDate;
                }
                var endedActions = task.actions.filter(function (action) { return action.files_id !== null; });
                if (endedActions.length > 0) {
                    task.taskEndDate = endedActions[0].endDate;
                }
            }
            return task;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["toArray"])());
    };
    TaskService.prototype.getBalance = function (participantId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]()
            .set('access_token', this.authService.accessToken.id)
            .set('filter', "{\"where\": \n        {\"and\": [ \n          {\"relationId\": " + participantId + "},\n          {\"statusId\": 34}\n        ]}\n      }");
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + "/Debts", { params: params })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    TaskService.prototype.getOne = function (uuid) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]()
            .set('access_token', this.authService.accessToken.id);
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + "/Tasks/" + uuid, { params: params })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    TaskService.prototype.getOneWhere = function (taskId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]()
            .set('access_token', this.authService.accessToken.id)
            .set('filter', "{\"where\": {\"taskId\": " + taskId + "}}");
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + "/Tasks", { params: params })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res[0]; }));
    };
    TaskService.prototype.updateTask = function (taskId, fields) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]().set('access_token', this.authService.accessToken.id);
        return this.http.patch(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + "/Tasks/" + taskId, fields, { params: params })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    TaskService.prototype.startTask = function (taskId) {
        return this.updateTaskStatus(taskId, 35, "START");
    };
    TaskService.prototype.pauseTask = function (taskId) {
        return this.updateTaskStatus(taskId, null, "PAUSE");
    };
    TaskService.prototype.completeTask = function (taskId, reason, imgBase64) {
        var _this = this;
        if (reason === void 0) { reason = ''; }
        var file = this.convertToFile(imgBase64);
        return this.uploadImage(taskId, file, reason)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["mergeMap"])(function (res) { return _this.updateTaskStatus(taskId, 33, reason); }));
    };
    TaskService.prototype.convertToFile = function (imgBase64) {
        var base64 = imgBase64.split(',')[1];
        var byteString = atob(base64);
        var arrayBuffer = new ArrayBuffer(byteString.length);
        var int8Array = new Uint8Array(arrayBuffer);
        for (var i = 0; i < byteString.length; i++) {
            int8Array[i] = byteString.charCodeAt(i);
        }
        var blob = new Blob([arrayBuffer], { type: 'image/png' });
        return blob;
    };
    TaskService.prototype.uploadImage = function (taskId, blob, reason) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]()
            .set('access_token', this.authService.accessToken.id);
        var formData = new FormData();
        formData.append('taskId', '' + taskId);
        formData.append('file', blob);
        formData.append('description', reason);
        formData.append('effort', '0');
        formData.append('json', '{}');
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + "/files/upload", formData, { params: params })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    TaskService.prototype.updateTaskStatus = function (taskId, statusId, description) {
        if (statusId === void 0) { statusId = null; }
        if (description === void 0) { description = ''; }
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]()
            .set('access_token', this.authService.accessToken.id);
        var body;
        body = { taskId: taskId, description: description, statusId: statusId };
        // tslint:disable-next-line:max-line-length
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + "/Actions", body, { params: params })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    };
    TaskService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"]])
    ], TaskService);
    return TaskService;
}());



/***/ }),

/***/ "./src/app/activities/balance/balance.component.html":
/*!***********************************************************!*\
  !*** ./src/app/activities/balance/balance.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"center\">\n  <div class=\"row\">\n    <span>Je saldo is</span>\n  </div>\n  <div class=\"row\">\n      <div class=\"balance-circle\">\n          <div>\n            <span>{{balance}}</span>\n          </div>\n      </div>\n  </div>\n</div>\n<button mat-raised-button type=\"button\" routerLink=\"/activity/list\" color=\"primary\" class=\"but-left mx-auto font-weight-bold m-3\">Taken</button> "

/***/ }),

/***/ "./src/app/activities/balance/balance.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/activities/balance/balance.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".balance-circle {\n  border-radius: 50%;\n  border: 5px solid black;\n  width: 230px;\n  height: 230px;\n  background-color: #FFE9B7; }\n\n.balance-circle > div {\n  border-radius: 50%;\n  width: 190px;\n  height: 190px;\n  position: relative;\n  top: 7%;\n  left: 0%;\n  background-color: #FAB51D; }\n\n.balance-circle > div > span {\n  color: #FFFFFF;\n  position: relative;\n  top: 30%;\n  left: 25%;\n  font-size: 50px; }\n\n.center div, span {\n  margin: 0 auto; }\n\n.center > div {\n  padding-top: 40px; }\n\n.center span {\n  font-size: 25px;\n  padding-top: 50px; }\n\n.but-left {\n  position: absolute;\n  bottom: 15px;\n  left: 20px; }\n"

/***/ }),

/***/ "./src/app/activities/balance/balance.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/activities/balance/balance.component.ts ***!
  \*********************************************************/
/*! exports provided: BalanceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BalanceComponent", function() { return BalanceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../_services/task.service */ "./src/app/_services/task.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BalanceComponent = /** @class */ (function () {
    function BalanceComponent(route, taskService, authService) {
        this.route = route;
        this.taskService = taskService;
        this.authService = authService;
    }
    BalanceComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            _this.taskService.getBalance(_this.authService.accessToken.userId)
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])())
                .subscribe(function (balance) {
                _this.balance = balance.payedOffSc || 0;
            }, function (error) {
                console.log(error);
            });
        });
    };
    BalanceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-balance',
            template: __webpack_require__(/*! ./balance.component.html */ "./src/app/activities/balance/balance.component.html"),
            styles: [__webpack_require__(/*! ./balance.component.scss */ "./src/app/activities/balance/balance.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_task_service__WEBPACK_IMPORTED_MODULE_2__["TaskService"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"]])
    ], BalanceComponent);
    return BalanceComponent;
}());



/***/ }),

/***/ "./src/app/activities/cam-capturing/cam-capturing.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/activities/cam-capturing/cam-capturing.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [hidden]=\"!hideConfirmation\">\n  <div><video #video id=\"video\" autoplay></video></div>\n  <div><button id=\"snap\" (click)=\"capture()\">Capture Photo</button></div>\n  <canvas #canvas id=\"canvas\"></canvas>\n</div>\n<div [hidden]=\"hideConfirmation\">\n  <app-post-activity [captured]=\"captured\" [taskId]=\"taskId\" (recapture)=\"onRecapture()\"></app-post-activity>\n</div>"

/***/ }),

/***/ "./src/app/activities/cam-capturing/cam-capturing.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/activities/cam-capturing/cam-capturing.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-toolbar .mat-primary {\n  display: none; }\n\n#video {\n  background-color: #000000;\n  width: 100vw;\n  height: 100%; }\n\n#canvas {\n  display: none;\n  width: 100vw;\n  height: 100%; }\n\nbutton {\n  margin-left: 30vw;\n  margin-top: 10%;\n  background-color: orange;\n  border-radius: 14px;\n  width: 40vw;\n  height: 10vh;\n  color: white; }\n"

/***/ }),

/***/ "./src/app/activities/cam-capturing/cam-capturing.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/activities/cam-capturing/cam-capturing.component.ts ***!
  \*********************************************************************/
/*! exports provided: CamCapturingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CamCapturingComponent", function() { return CamCapturingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _post_activity_post_activity_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./post-activity/post-activity.component */ "./src/app/activities/cam-capturing/post-activity/post-activity.component.ts");
/* harmony import */ var _services_alert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../_services/alert.service */ "./src/app/_services/alert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CamCapturingComponent = /** @class */ (function () {
    function CamCapturingComponent(router, route, alertService) {
        this.router = router;
        this.route = route;
        this.alertService = alertService;
    }
    CamCapturingComponent.prototype.ngOnInit = function () {
        this.taskId = +this.route.snapshot.paramMap.get('id');
        this.hideConfirmation = true;
    };
    CamCapturingComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
                //this.video.nativeElement.src = window.URL.createObjectURL(stream);
                _this.localstream = stream;
                _this.video.nativeElement.srcObject = stream;
                _this.video.nativeElement.play();
            });
        }
    };
    CamCapturingComponent.prototype.capture = function () {
        var context = this.canvas.nativeElement.getContext("2d").drawImage(this.video.nativeElement, 0, 0, 160, 130);
        this.captured = this.canvas.nativeElement.toDataURL('image/png');
        this.hideConfirmation = false;
        this.stopMediaTracks();
    };
    CamCapturingComponent.prototype.onRecapture = function () {
        this.hideConfirmation = true;
        this.ngAfterViewInit();
    };
    CamCapturingComponent.prototype.stopMediaTracks = function () {
        if (this.localstream && this.localstream.getTracks) {
            // getTracks() returns all media tracks (video+audio)
            this.localstream.getTracks()
                .forEach(function (track) { return track.stop(); });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_post_activity_post_activity_component__WEBPACK_IMPORTED_MODULE_2__["PostActivityComponent"]),
        __metadata("design:type", Object)
    ], CamCapturingComponent.prototype, "postActivityComponent", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('video'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], CamCapturingComponent.prototype, "video", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('canvas'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], CamCapturingComponent.prototype, "canvas", void 0);
    CamCapturingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cam-capturing',
            template: __webpack_require__(/*! ./cam-capturing.component.html */ "./src/app/activities/cam-capturing/cam-capturing.component.html"),
            styles: [__webpack_require__(/*! ./cam-capturing.component.scss */ "./src/app/activities/cam-capturing/cam-capturing.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_alert_service__WEBPACK_IMPORTED_MODULE_3__["AlertService"]])
    ], CamCapturingComponent);
    return CamCapturingComponent;
}());



/***/ }),

/***/ "./src/app/activities/cam-capturing/post-activity/post-activity.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/activities/cam-capturing/post-activity/post-activity.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container form\">\n  <div class=\"row\">\n    <div class=\"col-6\">\n      <img src=\"{{captured}}\">\n    </div>\n    <div class=\"col-6\">\n        <button mat-flat-button type=\"button\" (click)=\"clickAgain()\" class=\"mx-auto cancel m-3 float-right\">Opnieuw</button> \n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-12\">\n      <mat-form-field>\n        <textarea matInput placeholder=\"Typ hier je bericht\" [(ngModel)]=\"reason\"></textarea>\n      </mat-form-field>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-6\">\n      <button mat-flat-button type=\"button\" routerLink=\"/activity/list\" class=\"mx-auto m-3 cancel\">Annuleer</button> \n    </div>\n    <div class=\"col-6\">\n      <button mat-flat-button type=\"button\" (click)=completeTask() class=\"mx-auto m-3 finish\">Verstuur</button> \n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/activities/cam-capturing/post-activity/post-activity.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/activities/cam-capturing/post-activity/post-activity.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-form-field {\n  display: block; }\n\n.mat-button {\n  display: block; }\n\n.form {\n  padding-top: 30px;\n  padding-bottom: 40px; }\n\n.mat-flat-button {\n  width: 100%;\n  position: relative;\n  height: 60px;\n  font-size: 1.5rem; }\n\n.mat-flat-button.cancel {\n    background-color: #fff;\n    border: 1px solid #007bff;\n    color: #007bff; }\n\n.mat-flat-button.finish {\n    background-color: #28a745; }\n"

/***/ }),

/***/ "./src/app/activities/cam-capturing/post-activity/post-activity.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/activities/cam-capturing/post-activity/post-activity.component.ts ***!
  \***********************************************************************************/
/*! exports provided: PostActivityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostActivityComponent", function() { return PostActivityComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../_services/task.service */ "./src/app/_services/task.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PostActivityComponent = /** @class */ (function () {
    function PostActivityComponent(taskService, router) {
        this.taskService = taskService;
        this.router = router;
        this.recapture = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    PostActivityComponent.prototype.ngOnInit = function () {
    };
    PostActivityComponent.prototype.clickAgain = function () {
        this.recapture.emit();
    };
    PostActivityComponent.prototype.completeTask = function () {
        var _this = this;
        this.taskService.completeTask(this.taskId, this.reason, this.captured)
            .subscribe(function (data) {
            _this.router.navigate(['/activity/completed', _this.taskId]);
        }, function (error) {
            console.log(error);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PostActivityComponent.prototype, "captured", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], PostActivityComponent.prototype, "taskId", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], PostActivityComponent.prototype, "recapture", void 0);
    PostActivityComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-post-activity',
            template: __webpack_require__(/*! ./post-activity.component.html */ "./src/app/activities/cam-capturing/post-activity/post-activity.component.html"),
            styles: [__webpack_require__(/*! ./post-activity.component.scss */ "./src/app/activities/cam-capturing/post-activity/post-activity.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_task_service__WEBPACK_IMPORTED_MODULE_1__["TaskService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], PostActivityComponent);
    return PostActivityComponent;
}());



/***/ }),

/***/ "./src/app/activities/completed-activity/completed-activity.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/activities/completed-activity/completed-activity.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row title\">\n    <h2>Goed gedaan!</h2>\n  </div>\n  <div class=\"row logo\">\n    <img src=\"assets/icons-192.png\" alt=\"Social Coin\">\n  </div>\n  <div class=\"row text\">\n    <span>Bij het {{task?.taskName}} je {{task?.taskValueSc}} Social Coins verdiend!</span>\n  </div>\n  <div class=\"row button\">\n    <button mat-raised-button color=\"primary\" routerLink=\"/activity/list\">Taken</button>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/activities/completed-activity/completed-activity.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/activities/completed-activity/completed-activity.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  height: 100%;\n  justify-content: center; }\n\n.row {\n  flex-grow: 1;\n  width: 100%;\n  justify-content: center; }\n\n.row.title {\n    align-items: flex-end; }\n\n.row.title h2 {\n      font-weight: bold; }\n\n.row.logo {\n    align-items: center; }\n\n.row.logo img {\n      height: 200px; }\n\n.row.text span {\n    font-size: 1.3rem;\n    width: 80%;\n    text-align: center; }\n\n.row.button {\n    justify-content: flex-start; }\n\n.row.button button {\n      height: 60px;\n      width: 50%;\n      align-self: flex-end;\n      margin-bottom: 20px; }\n"

/***/ }),

/***/ "./src/app/activities/completed-activity/completed-activity.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/activities/completed-activity/completed-activity.component.ts ***!
  \*******************************************************************************/
/*! exports provided: CompletedActivityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompletedActivityComponent", function() { return CompletedActivityComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../_services/task.service */ "./src/app/_services/task.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CompletedActivityComponent = /** @class */ (function () {
    function CompletedActivityComponent(route, taskService) {
        this.route = route;
        this.taskService = taskService;
    }
    CompletedActivityComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.taskService.getOneWhere(+this.route.snapshot.paramMap.get('id'))
            .subscribe(function (task) {
            _this.task = task;
        });
    };
    CompletedActivityComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-completed-activity',
            template: __webpack_require__(/*! ./completed-activity.component.html */ "./src/app/activities/completed-activity/completed-activity.component.html"),
            styles: [__webpack_require__(/*! ./completed-activity.component.scss */ "./src/app/activities/completed-activity/completed-activity.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_task_service__WEBPACK_IMPORTED_MODULE_2__["TaskService"]])
    ], CompletedActivityComponent);
    return CompletedActivityComponent;
}());



/***/ }),

/***/ "./src/app/activities/detail-activity/detail-activity.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/activities/detail-activity/detail-activity.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"scvalue-container\"><img [src]=\" task.taskValueSc < 500 ? 'assets/singlecoin.png' : 'assets/multiplecoin.png' \"/><span class=\"scvalue-text\"> = {{task.taskValueSc}} Social Coins</span></div>\n  <div class=\"detail-text\">\n    {{task.taskDescription}}\n  </div>\n</div>\n  <div [hidden]=\"!hideProgress\">\n    <mat-list>\n      <mat-list-item *ngIf=\"task.taskStartDate || task.taskEndDate\">\n        <p matLine>\n          <span> {{ dateRange(task.taskStartDate, task.taskEndDate) }}</span>\n        </p>\n        <p matLine>\n          <span> {{ hourRange(task.taskStartDate, task.taskEndDate) }} </span>\n        </p>\n        <div class=\"img-container\">\n          <img src=\"assets/calendar.png\">\n        </div>\n      </mat-list-item>\n      <a mat-list-item href=\"geo:124.028582,-29.201930\" *ngIf=\"task.locationStreetname\">\n          <p matLine>\n            <span>{{task.locationStreetname}}</span>\n          </p>\n          <div class=\"img-container\">\n            <img src=\"assets/map.png\"/>\n          </div>\n        </a>\n      <a mat-list-item href=\"mailto:{{task.taskEmail}}\" *ngIf=\"task.taskEmail\">\n        <p matLine>\n          <span>{{task.taskEmail}}</span>\n        </p>\n        <div class=\"img-container\">\n          <img src=\"assets/mail.png\"/>\n        </div>\n      </a>\n      <a mat-list-item href=\"tel:{{task.taskPhonenumber}}\" *ngIf=\"task.taskPhonenumber\">\n        <p matLine>\n            <span>{{task.taskPhonenumber}}</span>\n        </p>\n        <div class=\"img-container\">\n          <img src=\"assets/phone.png\"/>\n        </div>\n      </a>\n    </mat-list>\n    <button mat-raised-button type=\"button\" (click)=\"clickStart()\" class=\"m-3 start\">Start</button> \n  </div>\n\n  <div *ngIf=\"!hideProgress\">\n    <app-progress-activity [taskId]=\"task.taskId\"></app-progress-activity>\n  </div>"

/***/ }),

/***/ "./src/app/activities/detail-activity/detail-activity.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/activities/detail-activity/detail-activity.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".scvalue-container {\n  position: relative;\n  text-align: center;\n  padding-bottom: 20px;\n  padding-top: 20px; }\n\n.scvalue-text {\n  position: relative;\n  top: 7px;\n  font-size: 30px;\n  color: #FF9A10; }\n\n.detail-text {\n  font-weight: 100;\n  font-size: 1rem;\n  padding-bottom: 40px; }\n\n.center div, span {\n  margin: 0 auto; }\n\n.mat-raised-button {\n  height: 60px;\n  width: 50%;\n  float: right; }\n\n.mat-raised-button.start {\n    background-color: #007bff;\n    color: #FFFFFF;\n    font-size: 35px; }\n\n.mat-list-item {\n  border-bottom: 1px solid #ccc;\n  height: 90px !important;\n  color: #9c9c9c !important; }\n\nmat-list-item img {\n  margin: 10px; }\n\n.mat-list .mat-list-item {\n  margin: 0 auto 0 0;\n  border-radius: 0; }\n\n.mat-list .mat-list-item .mat-list-item-content {\n  flex-direction: row !important; }\n\n.img-container {\n  text-align: center;\n  width: 130px;\n  min-width: 130px; }\n"

/***/ }),

/***/ "./src/app/activities/detail-activity/detail-activity.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/activities/detail-activity/detail-activity.component.ts ***!
  \*************************************************************************/
/*! exports provided: DetailActivityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailActivityComponent", function() { return DetailActivityComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../_services/task.service */ "./src/app/_services/task.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _models_task__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../_models/task */ "./src/app/_models/task.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DetailActivityComponent = /** @class */ (function () {
    function DetailActivityComponent(taskService, route) {
        this.taskService = taskService;
        this.route = route;
        this.task = new _models_task__WEBPACK_IMPORTED_MODULE_3__["Task"]();
    }
    DetailActivityComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.hideProgress = true;
        var id = +this.route.snapshot.paramMap.get('id');
        this.taskService.getOneWhere(id)
            .subscribe(function (task) {
            _this.task = task;
            if (task.statusId !== 30) {
                _this.hideProgress = false;
            }
        });
    };
    DetailActivityComponent.prototype.clickStart = function () {
        var _this = this;
        this.taskService.startTask(this.task.taskId)
            .subscribe(function (data) {
            window.scroll(0, 0);
            _this.hideProgress = false;
        }, function (error) {
            console.log(error);
        });
    };
    DetailActivityComponent.prototype.dateRange = function (taskStartDate, taskEndDate) {
        var startDateObj = new Date(taskStartDate);
        var endDateObj = new Date(taskEndDate);
        var convertedStartDate = '';
        if (startDateObj.toDateString() === endDateObj.toDateString()) {
            var startDate = startDateObj.toDateString().split(' ');
            // removing year
            startDate.pop();
            convertedStartDate = startDate.join(' ');
        }
        else {
            var startDate = startDateObj.toDateString().split(' ');
            var endDate = endDateObj.toDateString().split(' ');
            // removing year
            startDate.pop();
            endDate.pop();
            convertedStartDate = startDate.join(' ') + ' t/m ' + endDate.join(' ');
        }
        return convertedStartDate;
    };
    DetailActivityComponent.prototype.hourRange = function (taskStartDate, taskEndDate) {
        var startDateObj = new Date(taskStartDate);
        var endDateObj = new Date(taskEndDate);
        return startDateObj.toTimeString().split(' ')[0] + ' - ' + endDateObj.toTimeString().split(' ')[0];
    };
    DetailActivityComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-detail-activity',
            template: __webpack_require__(/*! ./detail-activity.component.html */ "./src/app/activities/detail-activity/detail-activity.component.html"),
            styles: [__webpack_require__(/*! ./detail-activity.component.scss */ "./src/app/activities/detail-activity/detail-activity.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_task_service__WEBPACK_IMPORTED_MODULE_1__["TaskService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], DetailActivityComponent);
    return DetailActivityComponent;
}());



/***/ }),

/***/ "./src/app/activities/list-activities/list-activities.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/activities/list-activities/list-activities.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-list>\n  <!-- <mat-list-item class=\"reverse-direction\" *ngFor=\"let task of tasks\" [routerLink]=\" task.statusId === 31 || task.statusId === 35 ? ['/activity/detail',task.taskId] : []\"> -->\n  <mat-list-item class=\"reverse-direction\" *ngFor=\"let task of tasks\" [routerLink]=\"task.statusId !== 33 && task.statusId !== 34 && task.statusId !== 48 ? ['/activity/detail',task.taskId] : []\" [queryParams]=\"{title: task.taskName}\">\n    <h2 matLine> {{task.taskName}} </h2>\n    <p matLine>\n      <span [innerHTML]=\"dateRange(task.taskStartDate, task.taskEndDate)\"></span>\n      <br>\n      <span [innerHTML]=\"hourRange(task.taskStartDate, task.taskEndDate)\"></span>\n      <br>\n      <span *ngIf=\"task.statusId === 34\">SC : {{task.taskValueSc}}</span>\n    </p>\n    <img matListAvatar class=\"icon-status\" src=\"assets/singlecoin.png\" alt=\"Social Value\" *ngIf=\"task.taskValueSc < 500  && task.statusId !== 34 && task.statusId !== 48\">\n    <img matListAvatar class=\"icon-status\" src=\"assets/multiplecoin.png\" alt=\"Social Value\" *ngIf=\"task.taskValueSc > 500  && task.statusId !== 34 && task.statusId !== 48\">\n    <img matListAvatar class=\"icon-status\" src=\"assets/completed.png\" alt=\"Completed\" *ngIf=\"task.statusId === 34\">\n    <img matListAvatar class=\"icon-status\" src=\"assets/rejected.png\" alt=\"Rejected\" *ngIf=\"task.statusId === 48\">\n  </mat-list-item>\n  <mat-list-item *ngIf=\"tasks.length === 0\" >\n    <h2 matLine> Gebruiker heeft geen taak </h2>\n  </mat-list-item>\n</mat-list>"

/***/ }),

/***/ "./src/app/activities/list-activities/list-activities.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/activities/list-activities/list-activities.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-list-item {\n  border-bottom: 1px solid #ccc;\n  height: 90px !important;\n  color: #9c9c9c !important; }\n\nmat-list-item h2 {\n  font-weight: bold !important;\n  margin-bottom: 10px !important;\n  color: #000; }\n\n.icon-status {\n  width: 50px !important;\n  height: 50px !important; }\n\n.mat-list .mat-list-item .mat-list-avatar {\n  border-radius: 0%;\n  width: auto; }\n\n.mat-list .mat-list-item .mat-list-item-content {\n  flex-direction: row-reverse !important; }\n\n.mat-list-item {\n  cursor: pointer; }\n"

/***/ }),

/***/ "./src/app/activities/list-activities/list-activities.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/activities/list-activities/list-activities.component.ts ***!
  \*************************************************************************/
/*! exports provided: ListActivitiesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListActivitiesComponent", function() { return ListActivitiesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../_services/task.service */ "./src/app/_services/task.service.ts");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ListActivitiesComponent = /** @class */ (function () {
    function ListActivitiesComponent(taskService, authService) {
        this.taskService = taskService;
        this.authService = authService;
        this.tasks = [];
    }
    ListActivitiesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.taskService.getByParticipant(this.authService.accessToken.userId)
            .subscribe(function (tasks) {
            _this.tasks = tasks;
        });
    };
    ListActivitiesComponent.prototype.dateRange = function (taskStartDate, taskEndDate) {
        if (taskStartDate === "") {
            return "";
        }
        var startDateObj = new Date(taskStartDate);
        var endDateObj = new Date(taskEndDate);
        var convertedStartDate = '';
        if (startDateObj.toDateString() === endDateObj.toDateString()) {
            var startDate = startDateObj.toDateString().split(' ');
            // removing year
            startDate.pop();
            convertedStartDate = startDate.join(' ');
        }
        else {
            var startDate = startDateObj.toDateString().split(' ');
            var endDate = endDateObj.toDateString().split(' ');
            // removing year
            startDate.pop();
            endDate.pop();
            convertedStartDate = startDate.join(' ') + ' t/m ' + endDate.join(' ');
        }
        return convertedStartDate;
    };
    ListActivitiesComponent.prototype.hourRange = function (taskStartDate, taskEndDate) {
        if (taskStartDate === "") {
            return "";
        }
        var startDateObj = new Date(taskStartDate);
        var endDateObj = new Date(taskEndDate);
        return startDateObj.toTimeString().split(' ')[0] + ' - ' + endDateObj.toTimeString().split(' ')[0];
    };
    ListActivitiesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-activities',
            template: __webpack_require__(/*! ./list-activities.component.html */ "./src/app/activities/list-activities/list-activities.component.html"),
            styles: [__webpack_require__(/*! ./list-activities.component.scss */ "./src/app/activities/list-activities/list-activities.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_task_service__WEBPACK_IMPORTED_MODULE_1__["TaskService"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"]])
    ], ListActivitiesComponent);
    return ListActivitiesComponent;
}());



/***/ }),

/***/ "./src/app/activities/progress-activity/finish-activity-confirmation/finish-activity-confirmation.component.html":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/activities/progress-activity/finish-activity-confirmation/finish-activity-confirmation.component.html ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Weet je het zeker?</h1>\n<mat-dialog-content>\n  <p>Tijd over: {{ passedData.progress }}</p>\n</mat-dialog-content>\n<mat-dialog-actions>\n  <button mat-button [mat-dialog-close]=\"true\" [routerLink]=\"['activity/camCapturing',taskId]\">Ja</button>\n  <button mat-button [mat-dialog-close]=\"false\">Nee</button>\n</mat-dialog-actions>"

/***/ }),

/***/ "./src/app/activities/progress-activity/finish-activity-confirmation/finish-activity-confirmation.component.scss":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/activities/progress-activity/finish-activity-confirmation/finish-activity-confirmation.component.scss ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/activities/progress-activity/finish-activity-confirmation/finish-activity-confirmation.component.ts":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/activities/progress-activity/finish-activity-confirmation/finish-activity-confirmation.component.ts ***!
  \*********************************************************************************************************************/
/*! exports provided: FinishActivityConfirmationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinishActivityConfirmationComponent", function() { return FinishActivityConfirmationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var FinishActivityConfirmationComponent = /** @class */ (function () {
    function FinishActivityConfirmationComponent(passedData) {
        this.passedData = passedData;
    }
    FinishActivityConfirmationComponent.prototype.ngOnInit = function () {
        this.taskId = this.passedData.taskId;
    };
    FinishActivityConfirmationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-finish-activity-confirmation',
            template: __webpack_require__(/*! ./finish-activity-confirmation.component.html */ "./src/app/activities/progress-activity/finish-activity-confirmation/finish-activity-confirmation.component.html"),
            styles: [__webpack_require__(/*! ./finish-activity-confirmation.component.scss */ "./src/app/activities/progress-activity/finish-activity-confirmation/finish-activity-confirmation.component.scss")]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object])
    ], FinishActivityConfirmationComponent);
    return FinishActivityConfirmationComponent;
}());



/***/ }),

/***/ "./src/app/activities/progress-activity/progress-activity.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/activities/progress-activity/progress-activity.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container center\">\n  <div class=\"row\">\n    <span class=\"font-weight-bold\">Totale tijd: {{ totalHours }} uur</span>\n  </div> \n\n  <div class=\"row position-relative\">\n    <mat-progress-spinner diameter=\"250\" strokeWidth=\"7\" class=\"progress-spinner\" [value]=\"progress\"></mat-progress-spinner>\n    <span class=\"readableTimer\">{{ readableTimer }}</span>\n  </div>  \n\n  <div class=\"row\">\n    <div class=\"col-6\">\n      <button mat-flat-button class=\"pause\" (click)=\"onPause()\">{{ pauseLabel }}</button>\n    </div>\n    <div class=\"col-6\">\n      <button mat-flat-button class=\"finish\" (click)=\"onFinish()\">KLAAR!</button>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/activities/progress-activity/progress-activity.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/activities/progress-activity/progress-activity.component.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".progress-spinner {\n  margin: 20px auto 30px auto; }\n\n.center div, span {\n  margin: 0 auto; }\n\n.mat-flat-button {\n  width: 100%;\n  position: relative;\n  height: 60px;\n  bottom: 20px; }\n\n.mat-flat-button.pause {\n    background-color: #007bff; }\n\n.mat-flat-button.finish {\n    background-color: #28a745; }\n\n.readableTimer {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%); }\n"

/***/ }),

/***/ "./src/app/activities/progress-activity/progress-activity.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/activities/progress-activity/progress-activity.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ProgressActivityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgressActivityComponent", function() { return ProgressActivityComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _finish_activity_confirmation_finish_activity_confirmation_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./finish-activity-confirmation/finish-activity-confirmation.component */ "./src/app/activities/progress-activity/finish-activity-confirmation/finish-activity-confirmation.component.ts");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../_services/task.service */ "./src/app/_services/task.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProgressActivityComponent = /** @class */ (function () {
    function ProgressActivityComponent(dialog, taskService, router) {
        this.dialog = dialog;
        this.taskService = taskService;
        this.router = router;
        this.progress = 100;
        this.isPaused = false;
        this.pauseLabel = 'PAUZE';
        this.activityDuration = 10800; // 3 hours = 180 minutes = 10800 seconds
    }
    ProgressActivityComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.taskService.getOneWhere(this.taskId).subscribe(function (data) {
            var actions = data.actions.filter(function (x) {
                return x.description === "START" || x.description === "PAUSE";
            });
            if (actions.length > 1) {
                console.log(actions);
                var dateNew_1 = new Date().getTime();
                var dateOld_1;
                var duration_1 = 0;
                var last_1;
                actions.map(function (item, i) {
                    if (item.description === "START") {
                        dateOld_1 = new Date(item.creationDate).getTime();
                        last_1 = item.description;
                    }
                    else {
                        dateNew_1 = new Date(item.creationDate).getTime();
                        duration_1 = ((dateNew_1 - dateOld_1) / 1000) + duration_1;
                        last_1 = item.description;
                    }
                });
                if (last_1 === "PAUSE") {
                    clearInterval(_this.timer);
                    _this.pauseLabel = 'GA VERDER';
                    _this.isPaused = !_this.isPaused;
                    _this.activityDuration = _this.activityDuration - duration_1;
                    _this.readableTimer = _this.secondsToHms(_this.activityDuration); // Remaing time
                    _this.totalHours = _this.secondsToHms(_this.activityDuration); // Total time
                }
                else {
                    _this.setActivity(_this.activityDuration - duration_1);
                }
            }
            else {
                var dateNew = new Date().getTime();
                var dateOld = new Date(actions[0].creationDate).getTime();
                _this.setActivity(_this.activityDuration - ((dateNew - dateOld) / 1000));
            }
        }, function (error) {
            console.error(error);
            _this.router.navigate(['/activity/list']);
        });
    };
    ProgressActivityComponent.prototype.setActivity = function (duration) {
        console.log("activityDuration", duration);
        if (!duration || duration < 0) {
            this.activityDuration = 0;
        }
        else {
            this.activityDuration = duration;
        }
        this.readableTimer = this.secondsToHms(this.activityDuration); // Remaing time
        this.totalHours = this.secondsToHms(this.activityDuration); // Total time
        this.startOrResumeActivity();
    };
    ProgressActivityComponent.prototype.startOrResumeActivity = function () {
        var _this = this;
        var step = (100 / this.activityDuration);
        this.timer = window.setInterval(function () {
            _this.progress = _this.progress - step;
            _this.readableTimer = _this.secondsToHms(_this.activityDuration * (_this.progress / 100));
            if (_this.progress <= 0) {
                clearInterval(_this.timer);
            }
        }, 1000);
    };
    ProgressActivityComponent.prototype.onPause = function () {
        var _this = this;
        if (this.isPaused === false) {
            this.taskService.pauseTask(this.taskId)
                .subscribe(function (data) {
                clearInterval(_this.timer);
                _this.pauseLabel = 'GA VERDER';
                _this.isPaused = !_this.isPaused;
            }, function (error) {
                console.error(error);
            });
        }
        else {
            this.taskService.startTask(this.taskId)
                .subscribe(function (data) {
                _this.startOrResumeActivity();
                _this.pauseLabel = 'PAUZE';
                _this.isPaused = !_this.isPaused;
            }, function (error) {
                console.error(error);
            });
        }
    };
    ProgressActivityComponent.prototype.onFinish = function () {
        var _this = this;
        clearInterval(this.timer);
        var dialogRef = this.dialog.open(_finish_activity_confirmation_finish_activity_confirmation_component__WEBPACK_IMPORTED_MODULE_2__["FinishActivityConfirmationComponent"], {
            data: {
                progress: this.readableTimer,
                taskId: this.taskId
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result) {
                console.log('go to next screen');
            }
            else {
                _this.startOrResumeActivity();
            }
        });
    };
    ProgressActivityComponent.prototype.secondsToHms = function (d) {
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);
        var hDisplay = (h > 0 ? (h < 10 ? '0' + h : h) : '00');
        var mDisplay = (m > 0 ? (m < 10 ? '0' + m : m) : '00');
        var sDisplay = (s > 0 ? (s < 10 ? '0' + s : s) : '00');
        return hDisplay + ':' + mDisplay + ':' + sDisplay;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], ProgressActivityComponent.prototype, "taskId", void 0);
    ProgressActivityComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-progress-activity',
            template: __webpack_require__(/*! ./progress-activity.component.html */ "./src/app/activities/progress-activity/progress-activity.component.html"),
            styles: [__webpack_require__(/*! ./progress-activity.component.scss */ "./src/app/activities/progress-activity/progress-activity.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _services_task_service__WEBPACK_IMPORTED_MODULE_3__["TaskService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], ProgressActivityComponent);
    return ProgressActivityComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _activities_list_activities_list_activities_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./activities/list-activities/list-activities.component */ "./src/app/activities/list-activities/list-activities.component.ts");
/* harmony import */ var _activities_detail_activity_detail_activity_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./activities/detail-activity/detail-activity.component */ "./src/app/activities/detail-activity/detail-activity.component.ts");
/* harmony import */ var _activities_balance_balance_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./activities/balance/balance.component */ "./src/app/activities/balance/balance.component.ts");
/* harmony import */ var _list_activities_toolbar_list_activities_toolbar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list-activities-toolbar/list-activities-toolbar.component */ "./src/app/list-activities-toolbar/list-activities-toolbar.component.ts");
/* harmony import */ var _back_to_last_page_toolbar_back_to_last_page_toolbar_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./back-to-last-page-toolbar/back-to-last-page-toolbar.component */ "./src/app/back-to-last-page-toolbar/back-to-last-page-toolbar.component.ts");
/* harmony import */ var _simple_toolbar_simple_toolbar_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./simple-toolbar/simple-toolbar.component */ "./src/app/simple-toolbar/simple-toolbar.component.ts");
/* harmony import */ var _activities_cam_capturing_cam_capturing_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./activities/cam-capturing/cam-capturing.component */ "./src/app/activities/cam-capturing/cam-capturing.component.ts");
/* harmony import */ var _activities_cam_capturing_post_activity_post_activity_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./activities/cam-capturing/post-activity/post-activity.component */ "./src/app/activities/cam-capturing/post-activity/post-activity.component.ts");
/* harmony import */ var _activities_completed_activity_completed_activity_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./activities/completed-activity/completed-activity.component */ "./src/app/activities/completed-activity/completed-activity.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./_guards/auth.guard */ "./src/app/_guards/auth.guard.ts");
/* harmony import */ var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./forgot-password/forgot-password.component */ "./src/app/forgot-password/forgot-password.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_11__["LoginComponent"], data: { toolbar: _simple_toolbar_simple_toolbar_component__WEBPACK_IMPORTED_MODULE_7__["SimpleToolbarComponent"] } },
    { path: 'forgot-password', component: _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_13__["ForgotPasswordComponent"], data: { toolbar: _simple_toolbar_simple_toolbar_component__WEBPACK_IMPORTED_MODULE_7__["SimpleToolbarComponent"] } },
    { path: 'activity/camCapturing/:id', component: _activities_cam_capturing_cam_capturing_component__WEBPACK_IMPORTED_MODULE_8__["CamCapturingComponent"], data: { toolbar: _simple_toolbar_simple_toolbar_component__WEBPACK_IMPORTED_MODULE_7__["SimpleToolbarComponent"] }, canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_12__["AuthGuard"]] },
    { path: 'activity/list', component: _activities_list_activities_list_activities_component__WEBPACK_IMPORTED_MODULE_2__["ListActivitiesComponent"], data: { toolbar: _list_activities_toolbar_list_activities_toolbar_component__WEBPACK_IMPORTED_MODULE_5__["ListActivitiesToolbarComponent"] }, canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_12__["AuthGuard"]] },
    { path: 'activity/detail/:id', component: _activities_detail_activity_detail_activity_component__WEBPACK_IMPORTED_MODULE_3__["DetailActivityComponent"], data: { toolbar: _back_to_last_page_toolbar_back_to_last_page_toolbar_component__WEBPACK_IMPORTED_MODULE_6__["BackToLastPageToolbarComponent"] }, canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_12__["AuthGuard"]] },
    { path: 'activity/balance', component: _activities_balance_balance_component__WEBPACK_IMPORTED_MODULE_4__["BalanceComponent"], data: { toolbar: _simple_toolbar_simple_toolbar_component__WEBPACK_IMPORTED_MODULE_7__["SimpleToolbarComponent"] }, canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_12__["AuthGuard"]] },
    { path: 'activity/post', component: _activities_cam_capturing_post_activity_post_activity_component__WEBPACK_IMPORTED_MODULE_9__["PostActivityComponent"], data: { toolbar: _simple_toolbar_simple_toolbar_component__WEBPACK_IMPORTED_MODULE_7__["SimpleToolbarComponent"] }, canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_12__["AuthGuard"]] },
    { path: 'activity/completed/:id', component: _activities_completed_activity_completed_activity_component__WEBPACK_IMPORTED_MODULE_10__["CompletedActivityComponent"], data: { toolbar: _simple_toolbar_simple_toolbar_component__WEBPACK_IMPORTED_MODULE_7__["SimpleToolbarComponent"] }, canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_12__["AuthGuard"]] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [hidden]=\"hideToolbar\">\n    <app-toolbar class=\"fixed-top\"></app-toolbar>\n</div>\n<div class=\"content\">\n    <app-alert></app-alert>\n    <router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content {\n  padding-top: 64px;\n  height: 100%; }\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./material.module */ "./src/app/material.module.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _toolbar_toolbar_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./toolbar/toolbar.component */ "./src/app/toolbar/toolbar.component.ts");
/* harmony import */ var _activities_list_activities_list_activities_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./activities/list-activities/list-activities.component */ "./src/app/activities/list-activities/list-activities.component.ts");
/* harmony import */ var _activities_detail_activity_detail_activity_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./activities/detail-activity/detail-activity.component */ "./src/app/activities/detail-activity/detail-activity.component.ts");
/* harmony import */ var _activities_balance_balance_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./activities/balance/balance.component */ "./src/app/activities/balance/balance.component.ts");
/* harmony import */ var _list_activities_toolbar_list_activities_toolbar_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./list-activities-toolbar/list-activities-toolbar.component */ "./src/app/list-activities-toolbar/list-activities-toolbar.component.ts");
/* harmony import */ var _simple_toolbar_simple_toolbar_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./simple-toolbar/simple-toolbar.component */ "./src/app/simple-toolbar/simple-toolbar.component.ts");
/* harmony import */ var _back_to_last_page_toolbar_back_to_last_page_toolbar_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./back-to-last-page-toolbar/back-to-last-page-toolbar.component */ "./src/app/back-to-last-page-toolbar/back-to-last-page-toolbar.component.ts");
/* harmony import */ var _activities_progress_activity_progress_activity_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./activities/progress-activity/progress-activity.component */ "./src/app/activities/progress-activity/progress-activity.component.ts");
/* harmony import */ var _activities_progress_activity_finish_activity_confirmation_finish_activity_confirmation_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./activities/progress-activity/finish-activity-confirmation/finish-activity-confirmation.component */ "./src/app/activities/progress-activity/finish-activity-confirmation/finish-activity-confirmation.component.ts");
/* harmony import */ var _activities_cam_capturing_cam_capturing_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./activities/cam-capturing/cam-capturing.component */ "./src/app/activities/cam-capturing/cam-capturing.component.ts");
/* harmony import */ var _activities_cam_capturing_post_activity_post_activity_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./activities/cam-capturing/post-activity/post-activity.component */ "./src/app/activities/cam-capturing/post-activity/post-activity.component.ts");
/* harmony import */ var _activities_completed_activity_completed_activity_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./activities/completed-activity/completed-activity.component */ "./src/app/activities/completed-activity/completed-activity.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./_services/authentication.service */ "./src/app/_services/authentication.service.ts");
/* harmony import */ var _services_alert_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./_services/alert.service */ "./src/app/_services/alert.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./_guards/auth.guard */ "./src/app/_guards/auth.guard.ts");
/* harmony import */ var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./forgot-password/forgot-password.component */ "./src/app/forgot-password/forgot-password.component.ts");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./_services/task.service */ "./src/app/_services/task.service.ts");
/* harmony import */ var _helpers_logging_interceptor__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./_helpers/logging.interceptor */ "./src/app/_helpers/logging.interceptor.ts");
/* harmony import */ var _directives_alert_alert_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./_directives/alert/alert.component */ "./src/app/_directives/alert/alert.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














// tslint:disable-next-line:max-line-length














var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _directives_alert_alert_component__WEBPACK_IMPORTED_MODULE_27__["AlertComponent"],
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _toolbar_toolbar_component__WEBPACK_IMPORTED_MODULE_6__["ToolbarComponent"],
                _activities_list_activities_list_activities_component__WEBPACK_IMPORTED_MODULE_7__["ListActivitiesComponent"],
                _activities_detail_activity_detail_activity_component__WEBPACK_IMPORTED_MODULE_8__["DetailActivityComponent"],
                _activities_balance_balance_component__WEBPACK_IMPORTED_MODULE_9__["BalanceComponent"],
                _list_activities_toolbar_list_activities_toolbar_component__WEBPACK_IMPORTED_MODULE_10__["ListActivitiesToolbarComponent"],
                _simple_toolbar_simple_toolbar_component__WEBPACK_IMPORTED_MODULE_11__["SimpleToolbarComponent"],
                _back_to_last_page_toolbar_back_to_last_page_toolbar_component__WEBPACK_IMPORTED_MODULE_12__["BackToLastPageToolbarComponent"],
                _activities_progress_activity_progress_activity_component__WEBPACK_IMPORTED_MODULE_13__["ProgressActivityComponent"],
                _activities_progress_activity_finish_activity_confirmation_finish_activity_confirmation_component__WEBPACK_IMPORTED_MODULE_14__["FinishActivityConfirmationComponent"],
                _activities_cam_capturing_cam_capturing_component__WEBPACK_IMPORTED_MODULE_15__["CamCapturingComponent"],
                _activities_cam_capturing_post_activity_post_activity_component__WEBPACK_IMPORTED_MODULE_16__["PostActivityComponent"],
                _activities_completed_activity_completed_activity_component__WEBPACK_IMPORTED_MODULE_17__["CompletedActivityComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_18__["LoginComponent"],
                _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_24__["ForgotPasswordComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_3__["MaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_19__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_22__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_19__["FormsModule"]
            ],
            providers: [
                _services_authentication_service__WEBPACK_IMPORTED_MODULE_20__["AuthenticationService"],
                _services_alert_service__WEBPACK_IMPORTED_MODULE_21__["AlertService"],
                _services_task_service__WEBPACK_IMPORTED_MODULE_25__["TaskService"],
                _guards_auth_guard__WEBPACK_IMPORTED_MODULE_23__["AuthGuard"],
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_22__["HTTP_INTERCEPTORS"], useClass: _helpers_logging_interceptor__WEBPACK_IMPORTED_MODULE_26__["LoggingInterceptor"], multi: true }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]],
            entryComponents: [_activities_progress_activity_finish_activity_confirmation_finish_activity_confirmation_component__WEBPACK_IMPORTED_MODULE_14__["FinishActivityConfirmationComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/back-to-last-page-toolbar/back-to-last-page-toolbar.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/back-to-last-page-toolbar/back-to-last-page-toolbar.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-icon class=\"back-button\" *ngIf=\"showBack()\" (click)=\"backClicked()\">arrow_back</mat-icon>\n<span>{{title}}</span>\n"

/***/ }),

/***/ "./src/app/back-to-last-page-toolbar/back-to-last-page-toolbar.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/back-to-last-page-toolbar/back-to-last-page-toolbar.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  width: 100%;\n  display: flex;\n  align-items: center; }\n\n.back-button {\n  cursor: pointer;\n  margin-right: 10px; }\n"

/***/ }),

/***/ "./src/app/back-to-last-page-toolbar/back-to-last-page-toolbar.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/back-to-last-page-toolbar/back-to-last-page-toolbar.component.ts ***!
  \**********************************************************************************/
/*! exports provided: BackToLastPageToolbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BackToLastPageToolbarComponent", function() { return BackToLastPageToolbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BackToLastPageToolbarComponent = /** @class */ (function () {
    function BackToLastPageToolbarComponent(_location, route) {
        this._location = _location;
        this.route = route;
    }
    BackToLastPageToolbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        // subscribe to router event
        this.route.queryParams.subscribe(function (params) {
            _this.title = params['title'];
            console.log(params);
        });
    };
    BackToLastPageToolbarComponent.prototype.showBack = function () {
        if (this._location.path()) {
            return true;
        }
        return false;
    };
    BackToLastPageToolbarComponent.prototype.backClicked = function () {
        this._location.back();
    };
    BackToLastPageToolbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-back-to-last-page-toolbar',
            template: __webpack_require__(/*! ./back-to-last-page-toolbar.component.html */ "./src/app/back-to-last-page-toolbar/back-to-last-page-toolbar.component.html"),
            styles: [__webpack_require__(/*! ./back-to-last-page-toolbar.component.scss */ "./src/app/back-to-last-page-toolbar/back-to-last-page-toolbar.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], BackToLastPageToolbarComponent);
    return BackToLastPageToolbarComponent;
}());



/***/ }),

/***/ "./src/app/forgot-password/forgot-password.component.html":
/*!****************************************************************!*\
  !*** ./src/app/forgot-password/forgot-password.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"card card-container\">\n      <div class=\"card-body\">\n        <form [formGroup]=\"forgotPasswordForm\" (ngSubmit)=\"onSubmit()\">\n          <div class=\"row\">\n            <mat-form-field>\n              <input matInput placeholder=\"Email\" formControlName=\"email\" required>\n            </mat-form-field>\n          </div>        \n          <div class=\"row\">\n            <button mat-raised-button color=\"primary\" class=\"mx-auto font-weight-bold m-3\">Send</button>\n          </div>\n          <div class=\"row justify-content-center\">\n            <img *ngIf=\"loading\" src=\"data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==\" />\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n  "

/***/ }),

/***/ "./src/app/forgot-password/forgot-password.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/forgot-password/forgot-password.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-container.card {\n  max-width: 270px;\n  padding: 40px 40px; }\n\n.card {\n  background-color: #F7F7F7;\n  padding: 20px 25px 30px;\n  margin: 0 auto 25px;\n  margin-top: 40px;\n  border-radius: 2px;\n  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3); }\n"

/***/ }),

/***/ "./src/app/forgot-password/forgot-password.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/forgot-password/forgot-password.component.ts ***!
  \**************************************************************/
/*! exports provided: ForgotPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordComponent", function() { return ForgotPasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services/alert.service */ "./src/app/_services/alert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ForgotPasswordComponent = /** @class */ (function () {
    function ForgotPasswordComponent(formBuilder, authService, router, alertService) {
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.router = router;
        this.alertService = alertService;
        this.loading = false;
        this.submitted = false;
    }
    ForgotPasswordComponent.prototype.ngOnInit = function () {
        this.forgotPasswordForm = this.formBuilder.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
        this.returnUrl = '/login';
    };
    ForgotPasswordComponent.prototype.onSubmit = function () {
        var _this = this;
        var _self = this;
        this.submitted = true;
        if (this.forgotPasswordForm.invalid) {
            return;
        }
        this.loading = true;
        this.authService.requestPasswordReset(this.forgotPasswordForm.value.email)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])())
            .subscribe(function (data) {
            _this.loading = false;
            _self.router.navigate([_self.returnUrl]);
            _self.alertService.success('A mail has been sent to ' + _this.forgotPasswordForm.value.email);
        }, function (error) {
            _self.loading = false;
            _self.router.navigate(['/login']);
            _self.alertService.error(error);
        });
    };
    ForgotPasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-forgot-password',
            template: __webpack_require__(/*! ./forgot-password.component.html */ "./src/app/forgot-password/forgot-password.component.html"),
            styles: [__webpack_require__(/*! ./forgot-password.component.scss */ "./src/app/forgot-password/forgot-password.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"]])
    ], ForgotPasswordComponent);
    return ForgotPasswordComponent;
}());



/***/ }),

/***/ "./src/app/list-activities-toolbar/list-activities-toolbar.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/list-activities-toolbar/list-activities-toolbar.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <mat-icon class=\"back-button\" *ngIf=\"showBack()\" (click)=\"backClicked()\" class>arrow_back</mat-icon>\n  <span>Activiteiten</span>\n  <!-- This fills the remaining space of the current row -->\n  <span class=\"fill-remaining-space\"></span>\n\n  <!-- <button mat-button routerLink=\"/activity/balance\">SALDO</button> -->"

/***/ }),

/***/ "./src/app/list-activities-toolbar/list-activities-toolbar.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/list-activities-toolbar/list-activities-toolbar.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  width: 100%;\n  display: flex; }\n\n.fill-remaining-space {\n  /* This fills the remaining space, by using flexbox. \n        Every toolbar row uses a flexbox row layout. */\n  /* flexbox brokes after put dynamic toolbar*/\n  flex: 1 1 auto; }\n\n.back-button {\n  cursor: pointer; }\n"

/***/ }),

/***/ "./src/app/list-activities-toolbar/list-activities-toolbar.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/list-activities-toolbar/list-activities-toolbar.component.ts ***!
  \******************************************************************************/
/*! exports provided: ListActivitiesToolbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListActivitiesToolbarComponent", function() { return ListActivitiesToolbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListActivitiesToolbarComponent = /** @class */ (function () {
    function ListActivitiesToolbarComponent(_location) {
        this._location = _location;
    }
    ListActivitiesToolbarComponent.prototype.ngOnInit = function () {
        console.log();
    };
    ListActivitiesToolbarComponent.prototype.showBack = function () {
        if (this._location.path() !== '/activity/list') {
            return true;
        }
        return false;
    };
    ListActivitiesToolbarComponent.prototype.backClicked = function () {
        this._location.back();
    };
    ListActivitiesToolbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-activities-toolbar',
            template: __webpack_require__(/*! ./list-activities-toolbar.component.html */ "./src/app/list-activities-toolbar/list-activities-toolbar.component.html"),
            styles: [__webpack_require__(/*! ./list-activities-toolbar.component.scss */ "./src/app/list-activities-toolbar/list-activities-toolbar.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"]])
    ], ListActivitiesToolbarComponent);
    return ListActivitiesToolbarComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"card card-container\">\n    <div class=\"card-body\">\n      <form [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit($event)\">\n        <div class=\"row\">\n          <mat-form-field>\n            <input matInput placeholder=\"Username\" formControlName=\"username\" required>\n          </mat-form-field>\n        </div>\n        <div class=\"row\">\n          <mat-form-field class=\"example-full-width\">\n            <input matInput placeholder=\"Password\" type=\"password\" formControlName=\"password\" required>\n          </mat-form-field>\n        </div>  \n        <div class=\"row\">\n          <button mat-raised-button type=\"submit\" color=\"primary\" class=\"mx-auto font-weight-bold m-3\">Login</button>\n        </div>\n        <div class=\"row\">\n          <a routerLink=\"/forgot-password\">Forgot your pasword?</a>\n        </div>\n        <div class=\"row justify-content-center\">\n          <img *ngIf=\"loading\" src=\"data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==\" />\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card-container.card {\n  max-width: 270px;\n  padding: 40px 40px; }\n\n.card {\n  background-color: #F7F7F7;\n  padding: 20px 25px 30px;\n  margin: 0 auto 25px;\n  margin-top: 40px;\n  border-radius: 2px;\n  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3); }\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
/* harmony import */ var _services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services/alert.service */ "./src/app/_services/alert.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginComponent = /** @class */ (function () {
    function LoginComponent(formBuilder, router, authenticationService, alertService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.authenticationService = authenticationService;
        this.alertService = alertService;
        this.loading = false;
        this.submitted = false;
        this.updatePermissions = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _self = this;
        this.returnUrl = 'activity/list';
        this.authenticationService.isLoggedIn
            .subscribe(function (res) {
            if (res) {
                _self.router.navigate([_self.returnUrl]);
            }
        });
        this.loginForm = this.formBuilder.group({
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    };
    Object.defineProperty(LoginComponent.prototype, "f", {
        // convenience getter for easy access to form fields
        get: function () { return this.loginForm.controls; },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.onSubmit = function (event) {
        var _this = this;
        event.preventDefault();
        var _self = this;
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        this.loading = true;
        this.authenticationService.login(this.f.username.value, this.f.password.value)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])())
            .subscribe(function (data) {
            _this.loading = false;
            _self.router.navigate([_self.returnUrl]);
        }, function (error) {
            _self.alertService.error(error);
            _self.loading = false;
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], LoginComponent.prototype, "updatePermissions", void 0);
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"],
            _services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/material.module.ts":
/*!************************************!*\
  !*** ./src/app/material.module.ts ***!
  \************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatInputModule"]
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatInputModule"]
            ],
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/simple-toolbar/simple-toolbar.component.html":
/*!**************************************************************!*\
  !*** ./src/app/simple-toolbar/simple-toolbar.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<span class=\"fill-remaining-space\">{{title}}</span>\n"

/***/ }),

/***/ "./src/app/simple-toolbar/simple-toolbar.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/simple-toolbar/simple-toolbar.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  width: 100%;\n  display: flex;\n  text-align: center; }\n\n.fill-remaining-space {\n  /* This fills the remaining space, by using flexbox. \n        Every toolbar row uses a flexbox row layout. */\n  /* flexbox brokes after put dynamic toolbar*/\n  flex: 1 1 auto; }\n"

/***/ }),

/***/ "./src/app/simple-toolbar/simple-toolbar.component.ts":
/*!************************************************************!*\
  !*** ./src/app/simple-toolbar/simple-toolbar.component.ts ***!
  \************************************************************/
/*! exports provided: SimpleToolbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SimpleToolbarComponent", function() { return SimpleToolbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SimpleToolbarComponent = /** @class */ (function () {
    function SimpleToolbarComponent(_location) {
        this._location = _location;
    }
    SimpleToolbarComponent.prototype.ngOnInit = function () {
        if (this._location.path().match('/activity/balance/[0-9]+')) {
            this.title = 'Saldo';
        }
        else if (this._location.path().match('/activity/completed/[0-9]+')) {
            this.title = 'Social Coins';
        }
        else {
            this.title = '';
        }
    };
    SimpleToolbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-simple-toolbar',
            template: __webpack_require__(/*! ./simple-toolbar.component.html */ "./src/app/simple-toolbar/simple-toolbar.component.html"),
            styles: [__webpack_require__(/*! ./simple-toolbar.component.scss */ "./src/app/simple-toolbar/simple-toolbar.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"]])
    ], SimpleToolbarComponent);
    return SimpleToolbarComponent;
}());



/***/ }),

/***/ "./src/app/toolbar/toolbar.component.html":
/*!************************************************!*\
  !*** ./src/app/toolbar/toolbar.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"primary\">\n  <div #toolbarTarget></div>\n  <div class=\"col-4 text-right\" *ngIf=\"isLoggedIn\">\n      <button mat-button=\"\" routerLink=\"/activity/balance\">\n        <span class=\"mat-button-wrapper\">SALDO</span>\n      </button>\n    <mat-icon (click)=\"logout()\">exit_to_app</mat-icon>\n  </div>\n</mat-toolbar>"

/***/ }),

/***/ "./src/app/toolbar/toolbar.component.scss":
/*!************************************************!*\
  !*** ./src/app/toolbar/toolbar.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-icon {\n  cursor: pointer; }\n\n.text-right {\n  display: flex;\n  align-items: center; }\n"

/***/ }),

/***/ "./src/app/toolbar/toolbar.component.ts":
/*!**********************************************!*\
  !*** ./src/app/toolbar/toolbar.component.ts ***!
  \**********************************************/
/*! exports provided: ToolbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToolbarComponent", function() { return ToolbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ToolbarComponent = /** @class */ (function () {
    function ToolbarComponent(router, componentFactoryResolver, authService) {
        this.router = router;
        this.componentFactoryResolver = componentFactoryResolver;
        this.authService = authService;
        this.toolbarComponents = new Array();
        this.isLoggedIn = false;
    }
    ToolbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.isLoggedIn
            .subscribe(function (res) {
            _this.isLoggedIn = res;
        });
        this.routerEventSubscription = this.router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]) {
                _this.updateToolbarContent(_this.router.routerState.snapshot.root);
            }
        });
    };
    ToolbarComponent.prototype.ngOnDestroy = function () {
        this.routerEventSubscription.unsubscribe();
    };
    ToolbarComponent.prototype.updateToolbarContent = function (snapshot) {
        console.log("updateToolbarContent");
        this.clearToolbar();
        var toolbar = snapshot.data.toolbar;
        if (toolbar instanceof _angular_core__WEBPACK_IMPORTED_MODULE_0__["Type"]) {
            var factory = this.componentFactoryResolver.resolveComponentFactory(toolbar);
            var componentRef = this.toolbarTarget.createComponent(factory);
            this.toolbarComponents.push(componentRef);
        }
        for (var _i = 0, _a = snapshot.children; _i < _a.length; _i++) {
            var childSnapshot = _a[_i];
            this.updateToolbarContent(childSnapshot);
        }
    };
    ToolbarComponent.prototype.clearToolbar = function () {
        console.log("clearToolbar");
        this.toolbarTarget.clear();
        for (var _i = 0, _a = this.toolbarComponents; _i < _a.length; _i++) {
            var toolbarComponent = _a[_i];
            toolbarComponent.destroy();
        }
    };
    ToolbarComponent.prototype.logout = function () {
        var _this = this;
        this.authService.logout()
            .subscribe(function (res) {
            _this.router.navigate(['/login']);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('toolbarTarget', { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"] }),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"])
    ], ToolbarComponent.prototype, "toolbarTarget", void 0);
    ToolbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-toolbar',
            template: __webpack_require__(/*! ./toolbar.component.html */ "./src/app/toolbar/toolbar.component.html"),
            styles: [__webpack_require__(/*! ./toolbar.component.scss */ "./src/app/toolbar/toolbar.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"]])
    ], ToolbarComponent);
    return ToolbarComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    apiUrl: 'https://staging.socialcoin.nl/api'
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!************************************************************************!*\
  !*** multi (webpack)-dev-server/client?http://0.0.0.0:0 ./src/main.ts ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /opt/socialcoin/mobile/node_modules/webpack-dev-server/client/index.js?http://0.0.0.0:0 */"./node_modules/webpack-dev-server/client/index.js?http://0.0.0.0:0");
module.exports = __webpack_require__(/*! /opt/socialcoin/mobile/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map